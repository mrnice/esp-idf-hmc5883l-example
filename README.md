# Example of using hmc5883l driver

This is an example project for how to use the hmc5883l driver component
with the ESP32 ESP-IDF.

Export your IDF_PATH and toolchain path as usual e.g.

```sh
$ export $IDF_PATH=$HOME/path/to/esp-idf
$ export PATH="$PATH:$HOME/path/to/xtensa-esp32-elf/bin"
```

Clone this repository with:

```sh
$ git clone --recursive https://gitlab.com/mrnice/esp-idf-hmc5883l-example.git
```

Configure the sdkconfig with:

```sh
$ make menuconfig
```

Then you can use

```sh
$ make flash
```
To flash the example to your ESP32.

The example output should be something like:

```sh
$ socat /dev/ttyUSB0,b115200 STDOUT
Magnetic data: X:258.52 mG, Y:-220.80 mG, Z:-509.68 mG
...
```
